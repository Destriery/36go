head.ready(function() {
    // $(document).on("click", function(){
    // 	$(".js-popup").hide();
    // });

    // function scrollFixedElements() {
    //     var scroll_left = $(this).scrollLeft();
    //     $(".fixed-element").css({
    //         left: -scroll_left
    //     });
    // }
    // scrollFixedElements();
    // $(window).scroll(function(){
    //     scrollFixedElements()
    // });
    // dropdown
    $(document).click(function() {
        $(".js-select").removeClass("is-active");
        $(".js-select-list").slideUp(100);
    });

    // select list
    $("body").on("click", ".js-select", function(event) {
        event.stopPropagation();
    });
    $("body").on("click", ".js-select-text", function(event) {
        var select = $(this).parents(".js-select");
        if (select.hasClass("is-active")) {
            $(".js-select").removeClass("is-active");
            $(".js-select-list").slideUp(100);
        } else {
            $(".js-select").removeClass("is-active");
            $(".js-select-list").slideUp(100);
            select.toggleClass("is-active").find(".js-select-list").slideToggle(100);
        }

    });
    $("body").on("click", ".js-select-list li", function() {
        var val = $(this).attr("data-val");
        var text = $(this).text();
        var select = $(this).parents(".js-select");
        var selectList = $(this).parents(".js-select-list");
        select.find(".js-select-text").text(text);
        select.find("option").removeAttr("selected");
        select.find('option[value="' + val + '"]').attr("selected", "selected");
        selectList.find("li").removeClass("is-active");
        $(this).addClass("is-active");
        select.removeClass("is-active");
        selectList.slideUp(100);
        
        if ($(this).closest( '.spec-select' ).length > 0) {
          $('.service__form__list').hide();
          $('#pec' + val).show();
        }

    });
    function sort(){
        $('.js-sort').tablesorter({cssHeader: false, headers: { 0: { sorter: false}, 1: {sorter: false} }}); 
    }
      if($('.js-sort').length) {
        sort();
      }

      $('.js-nav a').on('click', function(){
        var section = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(section).offset().top - 10
        }, 500);
        return false;
      });
      
      
      $(document).on("click", ".show-more", function(){
        var previewBlock = $('.' + $(this).attr('data-preview'));
        var mainBlock = $('.' + $(this).attr('data-main'));
        previewBlock.hide();
        var needReInitBanner = $(this).closest('.other_serv_preview').length;
        mainBlock.fadeIn(300, function() {
          if ( needReInitBanner ) {
            ScrollLeftBanner.init()
          }
        });
        $(this).remove();
        return false;        
      });
      


      //scroll to report if needed
      if (window.reportScrollId != undefined && window.reportScrollId > 0) {
        var reportItem = $("#report-" + reportScrollId);
        if (!reportItem.length) {
          reportItem = $("#report-hidden-" + reportScrollId);
          $("#report-show-more-link").click();
        }
        if (reportItem.length) {
          $(window).scrollTop(reportItem.offset().top - ( $(window).height()/2 )); 
        }        
      }



      $('.page-ajax').bind('click', function() {
        if ($(this).hasClass('next-b')) {
          var href = $('.page-ajax:eq('+$(this).attr('data-next-page')+')').attr('href');
        } else {
          var href = $(this).attr('href');
        }
        var link = this;
        $.get(href, {1:1}, function(result) {
          var publications = $(result).find('#publications').html();
          $('#publications').html(publications);
          $('.page-ajax').removeClass('is-active');
          if ($(link).hasClass('next-b')) {
            var nextPage = $(link).attr('data-next-page') - 1;
            $('.page-ajax:eq('+nextPage+')').addClass('is-active');
          } else {
            $(link).addClass('is-active');
          }
          var currentIndex = $('.page-ajax.is-active').index();
          $('.next-b').attr('data-next-page', currentIndex + 1);
          if (currentIndex == $('.page-ajax').length - 1) {
            $('.next-b').hide();
          } else {
            $('.next-b').show();
          }
        })
        return false;
      });
      
      
      $('.feedback_form').bind('submit', function() {
        var data = {};
        data['name'] = $(this).find('input[name=name]').val();
        data['phone'] = $(this).find('input[name=phone]').val();
        data['city'] = $(this).find('select[name=city]').val();
        data['id_medi_center'] = $(this).find('input[name=id_medi_center]').val();
        data['date_visit'] = $(this).find('input[name=date_visit]').val();
        data['time_visit'] = $(this).find('select[name=time_visit]').val();
        var token = $('#token');
        data[token.attr('name')] = token.val();
        if ($(this).find('select[name=specialization]') != undefined) {
          data['specialization'] = $(this).find('select[name=specialization]').val();
          data['service'] = $(this).find('#service_'+data['specialization']+'').val();
        } else {
          data['service'] = $(this).find('select[name=service]').val();
        }
        var form = this;
        var isNewTopForm = $(form).hasClass('top-form');
        $('.form__footer').hide();
        $(form).find('.feedback_error, .feedback_process').remove();
        if (isNewTopForm) {
          $(form).append('<p class="feedback_process" style="display: none">Отправляем...</p>');
          $(form).find('input[type=submit]').prop('disabled', true).css('background', '#ccc');
        } else {
          $(form).append('<p class="feedback_process" style="display: none"><br><br>Отправляем...</p>');
          $(form).find('input[type=submit]').hide();
        }
        $(form).find('.feedback_process').fadeIn(200);
        $.post('/index/feedback/', data, function(result) {
          result = $.parseJSON(result);
          $(form).find('.feedback_process').hide();
          if (result['error']) {
            $(form).append('<p class="feedback_error" style="display: none">'+result['error']+'</p>');
            if (isNewTopForm) {
              $(form).find('input[type=submit]').prop('disabled', false).attr('style', '');
              $(form).find('.feedback_error').fadeIn(200);
            } else {
              $(form).find('input[type=submit]').fadeIn(200, function() {
                $(form).find('.feedback_error').fadeIn(200);
              });
            }
          } else {
            if (isNewTopForm) {
               $(form).append('<p class="feedback_success" style="display: none">Спасибо за Вашу заявку! Мы свяжемся с Вами в  самое ближайшее время</p>');
            } else {
               $(form).append('<p class="feedback_success" style="display: none"><br><br>Спасибо за Вашу заявку! Мы свяжемся с Вами в  самое ближайшее время</p>');
            }
            $(form).find('.feedback_success').fadeIn(200);
            yaCounter32872255.reachGoal('send_lead');
          }
        })
        return false;
      });
      
      
	function tabsLoad() {
	   $(".js-tabs-simple").each(function(){
		 if ($(this).find(".is-active").length) {
			var index = $(this).find(".is-active").index();
			$(this).next().find(".js-tabs-simple-content").eq(index).show();
		 }
		 else {
		   $(this).find("li").eq(0).addClass("is-active");
		   $(this).next().find(".js-tabs-simple-content").eq(0).show();
		 }
	   });
   }

   tabsLoad();
   $("body").on("click", ".js-tabs-simple a", function() {
			var tabs = $(this).parents(".js-tabs-simple");
			var tabsCont = tabs.next().find(".js-tabs-simple-content");
			var index = $(this).parent().index();
			tabs.find("li").removeClass("is-active");
      $(this).parent().addClass("is-active");
			tabsCont.hide();
			tabsCont.eq(index).show();
			return false;      
   });
   
   function initCustomScroll() {
    $(".js-custom-scroll").mCustomScrollbar({
      theme: "light-thick",
      scrollInertia: 200,
      mouseWheel:{
      preventDefault: true,
      scrollAmount: 150 }
    });     
   }
   initCustomScroll();
  
  $('body').on('click', 'a.sort-link', function() {
    var sort = $(this).attr('data-sort');
    var direction = $(this).attr('data-sort-direction');
    var els = [];
    var container = $('#mCSB_1_container').length ? $('#mCSB_1_container'): $('#mCSB_2_container');
    container.find('.address').each(function() {
      els.push(this);
    });
    els.sort(function(a, b) {
      var cr1 = parseInt($(a).attr('data-' + sort)); 
      var cr2 = parseInt($(b).attr('data-' + sort)); 
      if (direction == 'ASC') {
        return cr1 > cr2?1:-1;
      } else {
        return cr1 < cr2?1:-1;
      }
    });
    var html = '';
    for (key in els) {
      var el = $(els[key]);
      html += '<div class="address" data-price="' + el.attr('data-price') + '" data-best_stuff="' + el.attr('data-best_stuff') + '" data-is_24="' + el.attr('data-is_24') + '">' + el.html() + '</div>'
    }
    container.html(html);
    return false;    
  });
  
  $('body').on('click', 'a.go-link', function() {
    var section = '#bottom-container';
    $('html, body').animate({
      scrollTop: $(section).offset().top - 10
    }, 500);
    return false;
  });
  
      
      
  var ScrollLeftBanner = {
    elBefore: null,
    elAfter: null,
    xTop: 0,
    yTop: 0,
    xBottom: 0,
    yBottom: 0,
    parentEl: null,
    absPos: 0,
    wasCount: false,
    init: function() {
      ScrollLeftBanner.absPos = 0;
      this.elBefore = $('#left_banner').prev();
      if (this.elBefore.offset() == undefined) {
        this.elBefore = $('#left_banner').closest('div.l-col2');
      }
      this.elAfter = $('section.region');
      if (this.elAfter.offset() == undefined) {
        this.elAfter = $('section.main-form');
        this.yBottom = this.elAfter.offset().top -110;
      } else {
        this.yBottom = this.elAfter.offset().top;
      }
      this.xTop = this.elBefore.offset().left;
      this.yTop = this.elBefore.height() + this.elBefore.offset().top;
      console.log(this.yTop);
      this.yBottom = this.yBottom - $('#left_banner').height() - 
        parseInt(this.elAfter.css('padding-top')) - parseInt($('#left_banner').css('padding-top')) * 2;
      this.parentEl = $('#left_banner').closest('div.l-col2');
          $('#left_banner').css({
            position : 'absolute', 
            left: '0px',
            top: this.elBefore.offset().top - this.parentEl.offset().top + this.elBefore.height() + 20,
            width: this.elBefore.width()
          });
    },
    
    run: function() {
      var obj = this;
      $(window).scroll(function() {
        var scrollTop = $(this).scrollTop();
        if (scrollTop > obj.yTop && scrollTop < obj.yBottom) {
          ScrollLeftBanner.absPos = scrollTop - obj.parentEl.offset().top + 20;
          var leftOffset = obj.elBefore.offset().left;
          $('#left_banner').css({
            position : 'fixed', 
            left: leftOffset + 'px',
            top: '30px',
            width: obj.elBefore.width()
          });
          if (!obj.wasCount) {
            setTimeout(function() {$('.banner__count span').html(parseInt($('.banner__count span').html()) - 1)}, 3000);
            obj.wasCount = true;
          }
        } else if (ScrollLeftBanner.absPos > 0) {
          $('#left_banner').css({
            position : 'absolute', 
            left: '0px',
            top: ScrollLeftBanner.absPos,
            width: obj.elBefore.width()
          });
        }
      });
    }
  }
  
  if ($('#left_banner').length > 0) {
    ScrollLeftBanner.init();
    ScrollLeftBanner.run();
  }
  

  function initDatepicker() {
    $(".datepicker").datepicker({
      dateFormat: "dd.mm",
      monthNames: ['Январь', 'Февраль', 'Март', 'Апрель','Май', 'Июнь', 'Июль', 'Август', 'Сентябрь','Октябрь', 'Ноябрь', 'Декабрь'],
      dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
      firstDay: 1,
    });
  };  
  initDatepicker();

	$('.js-calendar').click(function() {
		$(".datepicker").datepicker("show");
	});
  
  
  $("body").on("click", ".js-popup", function() {
    var elMainParent = $(this).parent().parent();
    var centerName = elMainParent.find('.address__header').text();
    window.centerId = elMainParent.find('.address__header').attr('data-center-id');
    $('.popup-location').text(centerName);
    $('#center_feedback_center_id').val(centerId);
    $('#center_feedback_block_spec').html(drawSpecs(centerId));
    $('#center_feedback_block_service').html(drawServices(centerId, specMainId));
    
		$(".popup-wrap").toggleClass("is-active");
  });
  
    function chooseCity () {
        var cityCatalog = $(".popup-wrap-city-choose .city-choose__catalog");
        var cityLetters = cityCatalog.find(".city-choose__catalog__letter");
        var cityItems = cityCatalog.find("a");
        
        var columnNum = 5;
        var column = [[]],
            columnTemp = 0,
            itemLength = 0;
        var columnLength = cityItems.length/columnNum;
        [].forEach.call(cityLetters, function (item) {
            column[columnTemp].push(item);
            itemLength += $(item).find('a').length;
            if (itemLength > columnLength) {
                itemLength = 0;
                columnTemp += 1;
                column[columnTemp] = [];
            }
        });
        cityCatalog.empty();
        column.forEach(function (_array) {
            var colItem = $('<div>');
            colItem.addClass('col-item');
            _array.forEach(function (item) {
                colItem.append(item);
            });
            cityCatalog.append(colItem);
        });
               
        
        $("body").on("click", ".js-popup-city", function() {
            $(".popup-wrap-city-choose").toggleClass("is-active");
            $(".popup-wrap-city-choose").find('.city-choose__input').focus();
        });
        $("body").on("click", ".popup-wrap-city-choose .city-choose__catalog a", function() {
            var cityName = $(this).text();
            $(".header__city__current span").html(cityName);
            $(".js-close").trigger("click");
            $(".city-choose__input").val('').trigger('keyup');
        });
        $("body").on("keyup", ".popup-wrap-city-choose .city-choose__input", function() {
            var valueTemp = $(this).val(),
                value = [];
            valueTemp.split(' ').forEach(function (item) {
                value.push(item.slice(0, 1).toUpperCase() + item.slice(1));
            });
            value = value.join(' ');
        
            if (value.length > 1) {
                cityItems.addClass('hidden');
                cityItems.filter(":contains("+value+")").removeClass('hidden');
                
                cityLetters.addClass('hidden');
                cityLetters.find("a:not(.hidden)").parents('.city-choose__catalog__letter').removeClass('hidden');
            }
            else {
                $(".popup-wrap-city-choose .city-choose__catalog a").removeClass('hidden');
                cityLetters.removeClass('hidden');
            }
        });
    }
    chooseCity();
      
    $("body").on("click", "#center_feedback_block_spec .js-select-list li", function() {
        specMain = $(this).attr("data-val");
        specMainId  = $(this).attr("data-id");
        serviceMain = centerSpecData[window.centerId][specMainId]['services'][0];
        $('#center_feedback_block_service').html(drawServices(window.centerId , specMainId));
    });
  
  function drawSpecs(centerId) {
    var specStr = '';
    var specStrUl = '';
    var specStrSelect = '';
    var specMainText;
    if (specMain == undefined) {
      for (key in centerSpecData[centerId]) {
        specMainText = centerSpecData[centerId][key]['name'];
        break;
      }
    } else {
      specMainText = specMain;
    }
    for (key in centerSpecData[centerId]) {
      var specName = centerSpecData[centerId][key]['name'];
      specStrUl += '<li data-id="'+key+'" data-val="'+specName+'" '+(specName == specMainText?'class="is-active"':'')+'>'+specName+'</li>';
      specStrSelect += '<option value="'+specName+'" '+(specName == specMainText?'selected="selected"':'')+'>'+specName+'</option>';
    }
    specStr = '<span class="input select-special select__text js-select-text">'+specMainText+'</span>' +
       '<ul class="select__list js-select-list">'+specStrUl+'<ul>' + '<select id="center_feedback_spec">'+specStrSelect+'</select>';
    return specStr;
  }
  
  function drawServices(centerId, idSpec) {
    var serviceStr = '';
    var serviceStrUl = '';
    var serviceStrSelect = '';
    var counter = 0;
    if (idSpec == undefined || centerSpecData[centerId][idSpec] == undefined) {
      for (key in centerSpecData[centerId]) {
        idSpec = key;
        break;
      }
    }
    for (key in centerSpecData[centerId][idSpec]['services']) {
      var servName = centerSpecData[centerId][idSpec]['services'][key];
      if (serviceMain == undefined && !counter) {
        serviceMain = servName;
      }
      console.log(key);
      serviceStrUl += '<li data-val="'+servName+'" '+(servName == serviceMain?'class="is-active"':'')+'>'+servName+'</li>';
      serviceStrSelect += '<option value="'+servName+'" '+(servName == serviceMain?'selected="selected"':'')+'>'+servName+'</option>';
      counter++;
    }
    serviceStr = '<span class="input select-service select__text js-select-text">'+serviceMain+'</span>' +
       '<ul class="select__list js-select-list">'+serviceStrUl+'<ul>' + '<select id="center_feedback_service">'+serviceStrSelect+'</select>';
    return serviceStr;
  }
  
  $('body').on('click', '#center_feedback_button', function() {
        var data = {};
        data['name'] = $('#center_feedback_name').val();
        data['phone'] = $('#center_feedback_phone').val();
        data['city'] = $('#center_feedback_city_id').val();
        data['service'] = $('#center_feedback_service').val();
        data['specialization'] = $('#center_feedback_spec').val();
        data['id_medi_center'] = $('#center_feedback_center_id').val();
        data['date_visit'] = $('#center_feedback_date_visit').val();
        data['time_visit'] = $('#center_feedback_time_visit').val();
        var token = $('#token');
        data[token.attr('name')] = token.val();
        var form = $('#center_feedback_misc');
        $(form).find('#center_feedback_button').hide();
        $(form).find('.feedback_error, .feedback_process').remove();
        $(form).append('<p class="feedback_process" style="display: none"><br><br>Отправляем...</p>');
        $(form).find('.feedback_process').fadeIn(200);
        $.post('/index/feedback/', data, function(result) {
          result = $.parseJSON(result);
          $(form).find('.feedback_process').hide();
          if (result['error']) {
            $(form).append('<p class="feedback_error" style="display: none">'+result['error']+'</p>');
            $(form).find('#center_feedback_button').fadeIn(200, function() {
              $(form).find('.feedback_error').fadeIn(200);
            });
          } else {
            $(form).append('<p class="feedback_success" style="display: none"><br>Спасибо за Вашу заявку! Мы свяжемся с Вами в  самое ближайшее время</p>');
            $(form).find('.feedback_success').fadeIn(200);
            yaCounter32872255.reachGoal('send_lead');
          }
        })
        return false;    
  });
  
  
  $("body").on("click", ".js-close", function() {
    $(".popup-wrap").removeClass("is-active");
  });
  
    $('.js-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        arrows: true,
        dots: true
    });
    
    var was_popup_slick = false;


    $('.js-zoom').on('click', function(){
        $('body').addClass('fixed');
        $('.overlay').show();
        $('.popup_gallery').addClass('visible');
        if (!was_popup_slick) {
          $('.js-popup-slider').slick({
              arrows: true,
              infinite: true,
              speed: 500,
              fade: true,
              cssEase: 'linear'
          });
        }
        var slickItem = $(this).parents('.slick-slide');
        $('.js-popup-slider').slick( "slickGoTo", slickItem.index() );
        was_popup_slick = true;
        return false;
    });     


    $('body').on('click', '.js-close', function() {
        $('body').removeClass('fixed');
        $('.overlay').hide();
        $('.popup_gallery').removeClass('visible');
        return false;      
    });
    
    $('#ask-doctor-form').bind('submit', function() {
      var formEl = $(this);
      formEl.find('.loading').hide().fadeIn(200);
      formEl.find('.error, .result, button').hide();
      formEl.find('input, textarea').removeClass('error-input');
      $.post($(this).attr('action'), $( this ).serialize(), function(data) {
        data = $.parseJSON(data);
        formEl.find('.loading').hide();
        if (data.result == 'ok') {
          formEl.find('.success').fadeIn(200);
        } else {
          formEl.find('button').show();
          formEl.find('.error').text('Необходимо корректно заполнить все поля').fadeIn(200);
          for (key in data.errors) {
            $('#DoctorAnswer_' + key).addClass('error-input');
          }
        }
      })
      return false;
    })
  
   if (window.SHOW_ALL_CENTERS) {
     
     $.get('/page/allCenterList', {'page_id': window.pageId}, function(result) {
       result = $.parseJSON(result);
       $('#medi_center_block_detail').html(result['snippetDetail']);
       $('#medi_center_block_price').html(result['snippetPrice']);
       initCustomScroll();
       initDatepicker();
        if($('.js-sort').length) {
          sort();
        }
     })
     
   }
});


